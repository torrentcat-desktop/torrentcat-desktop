;aiu;

[v1.2.1]
Name = TorrentCat
ProductVersion = 1.2.1.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.2.1.0
Description =2021-03-11T15:25:39.7421881+03:00
Feature = ��������� ����������� ������ ����� �������� ��������� � ����������
Feature1 = ��������� �������� ������
Feature2 = ��������� ����������� �������� ���������������� ��������� �� magnet-�������
Feature3 = ��������� ��������� ������� ���� CEF ��� ���������� ��
Feature4 = ��������� �������� ��������
Feature5 = ��������� �������������� ������� � ������ (������� �����/������, ������, Esc), ��������� ������� ����
Enhancement = ��������������� magnet-������ � ������ ������
Enhancement1 = ������� ������� ��� ���������������� ��������
Enhancement2 = �������������� �������� �������� ������
Enhancement3 = �������� ������ ���������
BugFix = ���������� ������ ������ � �� Windows 7, 8
BugFix1 = ���������� ������ � �������  "��� ���������"
BugFix2 = ���������� ������ � �������  "��� ��������"
BugFix3 = ���������� �������� ��������� �� ����������� ������

[v1.2.0]
Name = TorrentCat
ProductVersion = 1.2.0.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.2.0.0

[v1.1.11]
Name = TorrentCat
ProductVersion = 1.1.11.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.11.0

[v1.1.10]
Name = TorrentCat
ProductVersion = 1.1.10.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.10.0

[v1.1.9]
Name = TorrentCat
ProductVersion = 1.1.9.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.9.0

[v1.1.8]
Name = TorrentCat
ProductVersion = 1.1.8.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.8.0

[v1.1.7]
Name = TorrentCat
ProductVersion = 1.1.7.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.7.0

[v1.1.5]
Name = TorrentCat
ProductVersion = 1.1.5.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.5.0

[v1.1.4]
Name = TorrentCat
ProductVersion = 1.1.4.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.4.0

[v1.1.3]
Name = TorrentCat
ProductVersion = 1.1.3.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.3.0

[TorrentCat v1.1.2]
Name = TorrentCat
ProductVersion = 1.1.2.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.2.0

[TorrentCat v.1.1.1]
Name = TorrentCat
ProductVersion = 1.1.1.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.1.0

[TorrentCat v1.1.0]
Name = TorrentCat
ProductVersion = 1.1.0.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.1.0.0

[TorrentCat v1.0.16]
Name = TorrentCat
ProductVersion = 1.0.16.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.0.16.0

[TorrentCat v1.0.15]
Name = TorrentCat
ProductVersion = 1.0.15.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.0.15.0

[TorrentCat v1.0.14]
Name = TorrentCat
ProductVersion = 1.0.14.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.0.14.0

[TorrentCat v1.0.13]
Name = TorrentCat
ProductVersion = 1.0.13.0
URL = http://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.0.13.0

[TorrentCat]
Name = TorrentCat
ProductVersion = 1.0.13.0
URL = https://update.torrentcat.org/downloads/TorrentCat.exe
Size = 2138624
SHA256 = AE933C7D1E536BDB1F032E5E77B22B66B67969F6597EF246724E4948A46DA0AD
MD5 = bd06af899a64e1cfa67f57ed71b313dd
ServerFileName = TorrentCat.exe
RegistryKey = HKUD\Software\OCP Technology LLC\TorrentCat\Version
Version = 1.0.12
